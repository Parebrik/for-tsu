# Для начала мы должны считать все вершины в списки смежности
inp = open("input.txt", "r")
count_of_description = int(inp.readline())
list_of_neighbors = [[] for _ in range(0, 27)]  # Создали пустые списки смежности
dict0 = {}


def letter_to_num(let):
    if dict0.get(let) == None:
        dict0[let] = len(dict0)
    return dict0[let]


# считали все вершины и их рёбра в списки смежности
# граф- не ориентированный
for _ in range(0, count_of_description):
    string = inp.readline().split()
    string.pop(0)
    u = string[0]
    string.pop(0)
    u = letter_to_num(u)
    for nei in string:
        nei = letter_to_num(nei)
        list_of_neighbors[u].append(nei)
        list_of_neighbors[nei].append(u)
print(list_of_neighbors)

count = int(inp.readline())
for _ in range(0, count):
    string = list(inp.readline().split())
    from0 = string[0]
    from0 = dict0[from0]
    to = string[1]
    to = dict0[to]

    distance = 0

    que = []
    start = from0
    que.append(start)

    black = 2
    grey =  1
    white = 0
    status = [white] * 27
    status[from0] = grey

    last_ver = start
    while len(que) != 0:

        u = que.pop(0)
        status[u] = black
        neighbors = list_of_neighbors[u]

        for nei in neighbors:
            if status[nei] == white and nei not in que:
                que.append(nei)
                status[nei] = grey

        if to in que:
            print(distance + 1, string)
            break

        if u == last_ver:
            last_ver = que[-1]
            distance += 1
    # print(status)
