#include <iostream>
#include <vector>


bool isPrime(int a)
{
	for (int i = 2; i < a; ++i)
	{
		if (a % i == 0)
		{
			return false;
			break;
		}
	}
	return true;
}


void printPrimes(int m)
{
	for (int i = 1; i <= m; ++i)
	{
		if (isPrime(i))
		{
			std::cout << i << " ";
		}
	}
	std::cout << std::endl;
}


void printPrimesRev(int m)
{
	for (int i = m; i >= 1; --i)
	{
		if (isPrime(i))
		{
			std::cout << i << " ";
		}
	}
	std::cout << std::endl;
}


void printBinary(int input)
{
	if (input == 0)
	{
		std::cout << "0 = 0" << std::endl;
		return;
	}
	if (input < 0)
	{
		std::cout << "Не верный ввод." << std::endl;
		return;
	}
	int a = input;
	std::vector<int> binary;
	while (a != 1)
	{
		binary.push_back(a % 2);
		a = a / 2;
	}
	binary.push_back(1);
	int i = 0;
	int j = binary.size() - 1;

	while (i < j)
	{
		int t = 0;
		t = binary[i];
		binary[i] = binary[j];
		binary[j] = t;
		++i;
		--j;
	}
	std::cout << input << " = ";
	for (int i = 0; i < binary.size(); ++i)
	{
		std::cout << binary[i];
	}
	std::cout << std::endl;
}


void menuOfFunctions()
{
	int par = 0;
	std::cout << "Hello my friend, which task do u want to solve?" << std::endl;
	std::cout << "1- func isPrime" << std::endl;
	std::cout << "2- func printPrimes" << std::endl;
	std::cout << "3- func PrintPrimesRev" << std::endl;
	std::cout << "4- func printBinary" << std::endl;

	std::cin >> par;

	switch (par)
	{
	case 1:
	{
		std::cout << "Input ur number" << std::endl;
		int a = 0;
		std::cin >> a;
		std::cout << isPrime(a);
		break;
	}
	case 2:
	{
		std::cout << "Input ur number" << std::endl;
		int a = 0;
		std::cin >> a;
		printPrimes(a);
		break;
	}
	case 3:
	{
		std::cout << "Input ur number" << std::endl;
		int a = 0;
		std::cin >> a;
		printPrimesRev(a);
		break;
	}
	case 4:
	{
		std::cout << "Input ur number" << std::endl;
		int a = 0;
		std::cin >> a;
		printBinary(a);
		break;
	}
	default:
		std::cout << "Did you like " << par << " more then others numbers?" << std::endl;
		std::cout << "We don't care. THIS IS ERROR!";
		break;
	}
}
