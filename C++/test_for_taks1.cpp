#include "pch.h"
#include "../For study/funcs of study.h"

TEST(TestCaseName, TestName) {
  EXPECT_EQ(1, 1);
  EXPECT_TRUE(true);
}

TEST(isPrime, isPrimeReturnFalse)
{
	EXPECT_FALSE(isPrime(4));
	EXPECT_FALSE(isPrime(10));
	EXPECT_FALSE(isPrime(20));
}

TEST(isPrime, isPrimeReturnTrue)
{
	EXPECT_TRUE(isPrime(5));
	EXPECT_TRUE(isPrime(7));
	EXPECT_TRUE(isPrime(11));
}

TEST(printBinary, printBinaryReturnCorrect)
{
	//EXPECT_EQ(1010, printBinary(10));

}