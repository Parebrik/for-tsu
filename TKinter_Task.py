# ver 0.6.4
import os
import csv
import time
import random
import datetime
import functools
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog as fd
root = Tk()  # Запуск окна
WIDTH = 800
HEIGHT = 600

SPREAD = 115

VELOCITY_OF_LINE = 6.5

LENGTH_OF_LINE = 100

CHOSEN_OPTION_OF_LINES = 0
# 0 from right to left; 1 from left to right
START_POSITION = [[WIDTH, HEIGHT // 2, WIDTH + LENGTH_OF_LINE, HEIGHT // 2],
                  [0, HEIGHT // 2, -LENGTH_OF_LINE, HEIGHT // 2]]
START_VECTOR = [1, -1]
START_POSITION = START_POSITION[CHOSEN_OPTION_OF_LINES]
START_VECTOR = START_VECTOR[CHOSEN_OPTION_OF_LINES]


class Lists:
    """
    Этот класс создан специально чтобы было меньше глобальных переменных, к которым надо часто обращаться
    """
    def __init__(self):
        self.lines = []
        self.check = []


class Controller:
    def __init__(self, canv):
        self.canv = canv

        self.name_of_subject = ""
        self.characteristics_of_test = ["N циклов", "скорость N в сек", "разброс N мм"]

        self.start_of_test = False
        self.count_of_cycles = 1
        self.max_possible_cycles = 0

        self.start = datetime.datetime.now()

        self.pos = [START_POSITION[0] - START_VECTOR * 10, HEIGHT // 2 - 10, START_POSITION[0] - START_VECTOR * 10,
                    HEIGHT // 2 + 10]
        self.cont = canv.create_rectangle(self.pos, outline="Red", tag="cont")
        self.update_posi()

        self.now_statistics_of_cycle = [0, 0, 0, 0]  # статистика текущего цикла
        #                      ЛП ВД ВК ОВ
        self.now_statistics_of_mistakes = ["--", 0]
        #                                  0+- Ошибка в мм
        self.global_statistics_of_cycle = []  # статистика всех циклов, используется для вывода
        self.global_statistics_of_mistakes = []  # статистика всех ошибок у каждого цикла, используется для вывода

        self.LatPer = True
        self.TimeMotion = False
        self.TimeCorr = False
        self.swap_line = False
        self.property_is_0plus = False  # Если характеристика ошибки 0+, то вычисляем ошибку в мм отдельно
        self.count_of_mm_in_1pix = int(
            ent_of_window_of_calibration_0.get()) / 100  # По умолчанию в данном entry сразу лежит число типа int

    def __str__(self):
        return "Controller"

    def get_coordi_obj(self, obj):
        return self.canv.coords(obj)

    def get_coordi_cont(self):
        return self.canv.coords(self.cont)

    def update_posi(self):
        dif = self.canv.coords(self.cont)[1] - coord_pl[1] + 10
        self.canv.move(self.cont, 0, -dif)


class Line:
    def __init__(self, canv, *args, tag="lines", width=2):
        self.canv = canv
        self.posi = [el for el in args]
        # colors = {0: "red", 1: "black", 2: "yellow", 3: "pink", 4: "gray", 5: "green", 6: "orange"}
        self.line = canv.create_line(self.posi, width=width,
                                     tags=tag)  # теги нужны для удаления с холста не нужных линий
        self.positions_of_mouse = []

    def __str__(self):
        return "Line"

    def update_posi(self):
        self.canv.move(self.line, START_VECTOR * -VELOCITY_OF_LINE, 0)

    def get_posi(self):
        """
        return a list of 4 coordinates: x1, y1, x2, y2
        """
        return self.canv.coords(self.line)

    def add_new_line(self):
        rand = random.randrange(-3, 10)
        last_line = self.get_posi()
        delta = datetime.datetime.now() - controller.start
        if delta.seconds >= 1 and delta.microseconds >= 800000:
            controller.swap_line = True
        if delta.seconds >= 4:
            rand = random.randrange(-4, -1)
        if rand < 0 and controller.swap_line:  # End of cycle. Start new cycle
            controller.count_of_cycles += 1
            controller.start = datetime.datetime.now()  # Начало отсчёта времени
            controller.LatPer = True
            controller.swap_line = False
            # Формирование статистики
            controller.global_statistics_of_cycle.append(controller.now_statistics_of_cycle)
            controller.global_statistics_of_mistakes.append(controller.now_statistics_of_mistakes)
            controller.now_statistics_of_mistakes = ["--", 0]
            controller.now_statistics_of_cycle = [0, 0, 0, 0]

            posi_center = START_POSITION
            posi_down = [last_line[2], SPREAD + posi_center[1], last_line[2] + START_VECTOR * LENGTH_OF_LINE,
                         SPREAD + posi_center[3]]
            posi_up = [last_line[2], posi_center[1] - SPREAD, last_line[2] + START_VECTOR * LENGTH_OF_LINE,
                       posi_center[3] - SPREAD]

            if self.get_posi()[3] != HEIGHT // 2:  # if not in center: move to center
                l = Line(self.canv, *posi_center, width=4)
                posi_between = [last_line[2], last_line[3], l.get_posi()[0], l.get_posi()[1]]
                l_between = Line(self.canv, *posi_between, width=4)
                list_of.lines.append(l_between)
            else:
                if self.get_posi()[3] != HEIGHT // 2 - SPREAD and rand % 2 == 0:  # Move to up
                    l = Line(self.canv, *posi_up, width=4)
                    posi_between = [last_line[2], last_line[3], l.get_posi()[0], l.get_posi()[1]]
                    l_between = Line(self.canv, *posi_between, width=4)
                    list_of.lines.append(l_between)
                else:  # Move to down
                    l = Line(self.canv, *posi_down, width=4)
                    posi_between = [last_line[2], last_line[3], l.get_posi()[0], l.get_posi()[1]]
                    l_between = Line(self.canv, *posi_between, width=4)
                    list_of.lines.append(l_between)
            list_of.lines.append(l)
        else:  # continue of cycle
            l = Line(self.canv,
                     *[last_line[2], last_line[3], last_line[2] + START_VECTOR * LENGTH_OF_LINE, last_line[3]], width=4)
            list_of.lines.append(l)


def try_to_int(entry, default="26"):
    try:
        value = int(entry.get())
        return True
    except ValueError:
        entry.delete(0, len(entry.get()))
        entry.insert(0, default)
        return False


def new_cycle(event):
    if try_to_int(ent_for_start_of_new_cycle_2):
        controller.global_statistics_of_cycle = []
        controller.global_statistics_of_mistakes = []
        controller.start_of_test = True
        controller.max_possible_cycles = int(ent_for_start_of_new_cycle_2.get())
        controller.name_of_subject = ent_for_start_of_new_cycle_1.get()

        if controller.max_possible_cycles != 20 and controller.max_possible_cycles != 50 and controller.max_possible_cycles != 100:
            controller.characteristics_of_test[0] = "N циклов"
        else:
            controller.characteristics_of_test[0] = str(controller.max_possible_cycles) + " циклов"

        if chosen_vel.get() == -1:
            controller.characteristics_of_test[1] = "Ускорение от 100 мм в сек до 150 мм в сек"
        else:
            controller.characteristics_of_test[1] = "Скорость " + str(chosen_vel.get()) + " мм в сек"

        controller.characteristics_of_test[2] = "Разброс " + str(chosen_spread.get()) + " мм"
        controller.start = datetime.datetime.now()
        lab_for_start_of_new_cycle_1.lower()
        lab_for_start_of_new_cycle_2.lower()

        ent_for_start_of_new_cycle_1.lower()
        ent_for_start_of_new_cycle_2.lower()
        but_for_start_of_new_cycle.lower()

        draw()


def draw():
    if controller.count_of_cycles > controller.max_possible_cycles + 1:
        # Нам не следует учитывать первый цикл, т.к. он не корректный и идёт сразу
        controller.global_statistics_of_cycle.pop(0)
        controller.global_statistics_of_mistakes.pop(0)

        write_csv_file(controller.name_of_subject)
        controller.global_statistics_of_cycle = []

        lab_for_start_of_new_cycle_1.lift()
        lab_for_start_of_new_cycle_2.lift()
        ent_for_start_of_new_cycle_1.lift()
        ent_for_start_of_new_cycle_2.lift()
        but_for_start_of_new_cycle.lift()

        controller.count_of_cycles = 1
        controller.max_possible_cycles = 0
        canv.delete("lines")
        canv.delete("check")
        init_lists()
    else:
        stats()
        optimisation_of_check_lines()
        controller.update_posi()

        if START_VECTOR * list_of.lines[-1].get_posi()[2] <= WIDTH - WIDTH * CHOSEN_OPTION_OF_LINES:
            list_of.lines[-1].add_new_line()

        for line in list_of.lines:
            line.update_posi()

        for line in list_of.check:
            line.update_posi()

        draw_check_lines(list_of.check[-1])
        canv.after(10, draw)


def stats():
    """
    Здесь собирается вся статистика каждого цикла
    """
    now_pos = list_of.check[-1].get_posi()[1]
    if controller.LatPer and len(list_of.check) >= 2:
        if list_of.check[-2].get_posi()[1] != list_of.check[-1].get_posi()[1]:
            dump = datetime.datetime.now() - controller.start
            dump = dump.seconds + round(dump.microseconds / 10 ** 6, 4)
            controller.now_statistics_of_cycle[0] = dump
            controller.LatPer = False
            controller.TimeMotion = True

    if controller.TimeMotion:
        # 0+
        if (abs(now_pos - list_of.lines[-1].get_posi()[1]) <= 3 or list_of.check[-2].get_posi()[1] <=
                list_of.lines[-1].get_posi()[1] <= now_pos
                or now_pos <= list_of.lines[-1].get_posi()[1] <= list_of.check[-2].get_posi()[1]):
            dump = datetime.datetime.now() - controller.start
            dump = dump.seconds + round(dump.microseconds / 10 ** 6, 4)
            dump -= controller.now_statistics_of_cycle[0]
            controller.now_statistics_of_cycle[1] = dump
            controller.TimeMotion = False
            controller.TimeCorr = True
            controller.now_statistics_of_mistakes[0] = "0+"
            controller.property_is_0plus = True
            # Попробуем сделать наивно- если встречаем, что две точки- тек и пред равны
            # Значит мы нашли место, где check линия становится паралельной, то есть пересекли главную линию и
            # остановились, для дальнейшего сближения с главной линией
            # Другой способ- мы можем обнаруживать вершину параболы, которая не исбежно появится при ошибке 0+

        # 0-
        if abs(now_pos - list_of.check[-2].get_posi()[1]) <= 0:
            dump = datetime.datetime.now() - controller.start
            dump = dump.seconds + round(dump.microseconds / 10 ** 6, 4)
            dump -= controller.now_statistics_of_cycle[0]
            controller.now_statistics_of_cycle[1] = dump
            controller.TimeMotion = False
            controller.TimeCorr = True
            controller.now_statistics_of_mistakes[0] = "0-"
            #  Просто вычислить расстояние в мм от *now_pos* до list_of.lines[-1].get_posi()[1]
            mistake_in_mm = abs(now_pos - list_of.lines[-1].get_posi()[1])  # в пикселях
            mistake_in_mm = translate_from_pix_to_mm(mistake_in_mm)  # в мм
            controller.now_statistics_of_mistakes[1] = round(mistake_in_mm, 2)

    if controller.property_is_0plus:
        if now_pos == list_of.check[-2].get_posi()[1]:
            mistake_in_mm = abs(now_pos - list_of.lines[-1].get_posi()[1])  # в пикселях
            mistake_in_mm = translate_from_pix_to_mm(mistake_in_mm)  # в мм
            controller.now_statistics_of_mistakes[1] = round(mistake_in_mm, 2)
            controller.property_is_0plus = False

    if controller.TimeCorr and len(list_of.check) >= 15:
        if list_of.check[-15].get_posi()[1] == list_of.check[-1].get_posi()[1] and abs(
                now_pos - list_of.lines[-1].get_posi()[1]) <= 5:
            dump = datetime.datetime.now() - controller.start
            dump = dump.seconds + round(dump.microseconds / 10 ** 6, 4)
            dump -= controller.now_statistics_of_cycle[0]
            dump -= controller.now_statistics_of_cycle[1]
            controller.now_statistics_of_cycle[2] = dump
            # Вычисляем ОВ
            controller.now_statistics_of_cycle[3] = functools.reduce(lambda x, y: x + y,
                                                                     controller.now_statistics_of_cycle)
            controller.TimeCorr = False
            controller.swap_line = True


def translate_from_pix_to_mm(pix):
    controller.count_of_mm_in_1pix = int(
        ent_of_window_of_calibration_0.get()) / 100  # обновим данное значение, вдруг пользователь его поменял
    mm = pix * controller.count_of_mm_in_1pix
    return mm


def mouse_coords(coordi):
    global coord_pl
    x = coordi.x
    y = coordi.y
    coord_pl = [x, y]


def draw_check_lines(last_line):
    last_posi = last_line.get_posi()
    new_line = Line(canv, *[last_posi[2], last_posi[3], WIDTH - WIDTH * CHOSEN_OPTION_OF_LINES, coord_pl[1]],
                    tag="check")
    list_of.check.append(new_line)


def optimisation_of_check_lines():
    """
    Функция для удаления "check" линий, которые уже вышли из зримой области, однако всё равно замедляли работу программы
    """
    global canv
    if len(list_of.check) == 701:
        canv.addtag("todel_check", "withtag", "check")
    if len(list_of.check) >= 1400:
        canv.delete("todel_check")
        list_of.check = list_of.check[700:]


root.bind("<Motion>", mouse_coords)
canv = Canvas(root, width=WIDTH, height=HEIGHT, bg="White")

canv.place(x=0)

coord_pl = [WIDTH / 2, HEIGHT]
list_of = Lists()


def init_lists():
    list_of.lines = []
    l = Line(canv, *START_POSITION, width=4)
    list_of.lines.append(l)

    list_of.check = []
    l = Line(canv, *START_POSITION)
    list_of.check.append(l)


def check_directory_for_tests(name=""):
    """
    :param name: используется для создания папки под именем испытуемого в папке 'Испытуемые'
    """
    path = os.getcwd()
    # Испытуемые
    # 20 циклов
    # 50 циклов
    # 100 циклов
    # 100 мм/с
    # 150 мм/с
    # от 100 мм/с до 150 мм/с
    if not os.path.exists(path + "/Испытуемые/" + name):
        os.makedirs(path + "/Испытуемые/" + name)
    if not os.path.exists(path + "/Средние значения/" + name):
        os.makedirs(path + "/Средние значения/" + name)

    if not os.path.exists(path + "/Тесты/20 циклов"):
        os.makedirs(path + "/Тесты/20 циклов")
    if not os.path.exists(path + "/Тесты/50 циклов"):
        os.makedirs(path + "/Тесты/50 циклов")
    if not os.path.exists(path + "/Тесты/100 циклов"):
        os.makedirs(path + "/Тесты/100 циклов")
    if not os.path.exists(path + "/Тесты/N циклов"):
        os.makedirs(path + "/Тесты/N циклов")

    if not os.path.exists(path + r"/Тесты/Скорость 100 мм в сек"):
        os.makedirs(path + r"/Тесты/Скорость 100 мм в сек")
    if not os.path.exists(path + r"/Тесты/Скорость 150 мм в сек"):
        os.makedirs(path + r"/Тесты/Скорость 150 мм в сек")
    if not os.path.exists(path + r"/Тесты/Скорость от 100 мм в сек до 150 мм в сек"):
        os.makedirs(path + r"/Тесты/Скорость от 100 мм в сек до 150 мм в сек")

    if not os.path.exists(path + r"/Тесты/Разброс 30 мм"):
        os.makedirs(path + r"/Тесты/Разброс 30 мм")
    if not os.path.exists(path + r"/Тесты/Разброс 50 мм"):
        os.makedirs(path + r"/Тесты/Разброс 50 мм")


def average(stats_cycles, stats_mstk):
    """
    На вход [статистика циклов], [статистика ошибки]

    Возвращает список средних значений
    [ЛП, ВД, ВК, ОВ], [0+-, мм]
    """
    i = 0
    average_cycle = [0, 0, 0, 0]
    #                ЛП ВД ВК ОВ
    average_mstk = ["", 0]
    #              Ошбк мм

    # Вычеслим среднее значение для ЛП ВД ВК ОВ
    for i in range(len(stats_cycles[0])):
        for el in stats_cycles:
            average_cycle[i] += el[i]
        average_cycle[i] /= len(stats_cycles)
        average_cycle[i] = round(average_cycle[i], 4)

    average_property = [0, 0]
    #                   0+ 0-
    average_mm = 0
    for el in stats_mstk:
        property_of_test = el[0]
        mm = el[1]
        average_mm += mm
        if property_of_test == "0+":
            average_property[0] += 1
        if property_of_test == "0-":
            average_property[1] += 1
    average_mm /= len(stats_mstk)
    average_mstk[1] = round(average_mm, 2)
    if average_property[0] > average_property[1]:
        average_mstk[0] = "0+"
    elif average_property[0] < average_property[1]:
        average_mstk[0] = "0-"
    else:
        average_mstk[0] = "0+ 0-"
    return average_cycle, average_mstk


def create_file(path_to_write, name, stat_cycle=None, stat_mstk=None):
    i = 2
    if os.path.exists(path_to_write + "/" + name + ".csv"):
        while True:
            if os.path.exists(path_to_write + "/" + name + " " + str(i) + ".csv"):
                i += 1
            else:
                name += " " + "(" + str(i) + ")"
                break

    path_to_write += "/" + name + ".csv"

    titles = ["Номер Цикла;Латентный период;Время движения;Время коррекции;Общее время;Ошибка движения"]
    if stat_cycle is None or stat_mstk is None:
        stat_cycle = controller.global_statistics_of_cycle[:]
        stat_mstk = controller.global_statistics_of_mistakes[:]
    with open(path_to_write, "w", newline="", encoding="windows-1251") as csv_file:
        # Если мы поставим кодировку Юникод, то ломается всё в самом excel
        writer = csv.writer(csv_file, delimiter=".")
        writer.writerow(titles)
        i = 1
        for el_cycle, el_mstk in zip(stat_cycle, stat_mstk):
            el_cycle = list(map(str, el_cycle))
            el_mstk = list(map(str, el_mstk))

            for j in range(len(el_cycle)):
                el_cycle[j] = el_cycle[j].replace('.', ',')
                el_cycle[j] += ";"
            for j in range(len(el_mstk)):
                el_mstk[j] = el_mstk[j].replace('.', ',')

            line_to_write = str(i) + ";" + \
                            el_cycle[0] + el_cycle[1] + el_cycle[2] + el_cycle[3] + el_mstk[0] + el_mstk[1] + " мм"
            writer.writerow([line_to_write])
            i += 1

        writer.writerow([""])
        titles_average = ["Всего циклов;Средний ЛП;Среднее ВД;Среднее ВК;Среднее ОВ;Средняя Ошибка"]
        writer.writerow(titles_average)

        average_stats_cycle, average_stats_of_mistakes = average(stat_cycle, stat_mstk)
        print(average_stats_cycle, average_stats_of_mistakes)
        average_stats_cycle = list(map(str, average_stats_cycle))
        average_stats_of_mistakes = list(map(str, average_stats_of_mistakes))
        for j in range(len(average_stats_cycle)):
            average_stats_cycle[j] = average_stats_cycle[j].replace('.', ',')
            average_stats_cycle[j] += ";"
        for j in range(len(average_stats_of_mistakes)):
            average_stats_of_mistakes[j] = average_stats_of_mistakes[j].replace('.', ',')

        line_to_write = str(len(stat_cycle)) + ";" \
                        + average_stats_cycle[0] + average_stats_cycle[1] + average_stats_cycle[2] + \
                        average_stats_cycle[3] \
                        + average_stats_of_mistakes[0] + average_stats_of_mistakes[1] + " мм"

        writer.writerow([line_to_write])


def average_stat_cycles_of_all_subjects():
    """
    Важно помнить, что в папке у всех испытуемых должны быть одинаковые циклы!
    """
    path = os.getcwd()
    directory = fd.askdirectory(initialdir=path + "/Тесты")
    if directory == "":
        return
    names_of_subjects = os.listdir(directory)
    row_statistic_of_all_subjects = []
    # row_statistic_of_all_subjects
    # [    [[cycle1], [cycle2], [cycleN]], [[cycle1], [cycle2], [cycleN]], [[cycle1], [cycle2], [cycleN]]   ]
    #                 subject1                        subject2                        subjectM
    for subject in names_of_subjects:
        with open(directory + "/" + subject, "r", encoding="windows-1251") as csv_file:
            now_subj = []
            for line in csv_file:
                cycle = line.split(";")
                cycle[-1] = cycle[-1].rstrip()
                now_subj.append(cycle)
            row_statistic_of_all_subjects.append(now_subj)

    # Проверим, что у все исп. одно кол-во циклов
    norm_len = len(row_statistic_of_all_subjects[0])
    for el in row_statistic_of_all_subjects:
        if len(el) != norm_len:
            messagebox.showerror("Ошибка", "У всех испытуемых кол-во циклов в тесте должно быть равно!")
            return

    # Начинаем формировать нужные значения
    stats_cycles_of_all_subj = []
    stats_mstk_of_all_subj = []
    N = len(row_statistic_of_all_subjects[0]) - 4  # 4 строки лишние и позже мы их удалим
    M = len(row_statistic_of_all_subjects)
    stats_cycles = [[[] for _ in range(M)] for _ in range(N)]
    stats_mstk = [[[] for _ in range(M)] for _ in range(N)]
    # Структура stats_cycles и stats_mstk
    # [    [[cycle1], [cycle1], [cycle1]], [[cycle2], [cycle2], [cycle2]], [[cycleN], [cycleN], [cycleN]]   ]
    #       subject1  subject2  subjectM    subject1  subject2  subjectM    subject1  subject2  subjectM
    for number, subj in enumerate(row_statistic_of_all_subjects):
        subj.pop(0)  # Строка с наименованием столбцов
        subj.pop(-3)  # Пустая строка
        subj.pop(-2)  # Строка с наименованием средних значений
        subj.pop(-1)  # Строка с ненужной средней статистикой
        for ind, cycle in enumerate(subj):
            cycle.pop(0)  # удалим номер строки (цикла)
            cycle[-1] = cycle[-1][:-3]  # удалим подпись "мм"
            mstk = cycle.pop()
            mstk = mstk.replace(",", ".")
            print(mstk, mstk[2:], type(mstk[2:]))
            mstk = [mstk[:2], float(mstk[2:])]
            stats_mstk_of_all_subj.append(mstk)
            cycle_to_add = []
            for parametr in cycle:
                parametr = parametr.replace(",", ".")
                parametr = float(parametr)
                cycle_to_add.append(parametr)
            stats_cycles_of_all_subj.append(cycle_to_add)
            stats_cycles[ind][number] = cycle_to_add[:]
            stats_mstk[ind][number] = mstk[:]

    average_of_all_cycles = []
    average_of_all_mstk = []
    for cycle, mstk in zip(stats_cycles, stats_mstk):
        cur_average_cycle, cur_average_mstk = average(cycle, mstk)
        average_of_all_cycles.append(cur_average_cycle)
        average_of_all_mstk.append(cur_average_mstk)

    path_to_write = os.getcwd() + "/Средние значения"
    # Сформируем название файла:
    name = ""
    for ind, el in enumerate(directory[::-1]):
        if el == "/":
            name = directory[-ind - 1:]
            break
    create_file(path_to_write, name, average_of_all_cycles, average_of_all_mstk)


def write_csv_file(name):
    if name == "":
        name = "Без имени"
    # Создаём три одинаковых файла в дериктории 'Тесты' по имени *characteristics_of_test[i]*
    for i in range(len(controller.characteristics_of_test)):
        path_to_write = os.getcwd()
        path_to_write += "/Тесты/" + controller.characteristics_of_test[i]
        create_file(path_to_write, name)
    # Создаём файлик по пути 'Испытуемые // *имя испытуемого* '

    check_directory_for_tests(name)
    path_to_write = os.getcwd()
    path_to_write += "/Испытуемые/" + name

    name_of_file = str(time.localtime()[2]) + "." + str(time.localtime()[1]) + "." + str(time.localtime()[0])
    create_file(path_to_write, name_of_file)


#  Main menu:
lab_for_start_of_new_cycle_1 = Label(master=root, text="Введите свои фамилию и имя:")
lab_for_start_of_new_cycle_1.place(x=WIDTH // 3, y=140)

ent_for_start_of_new_cycle_1 = Entry(master=root, bg="grey")
ent_for_start_of_new_cycle_1.place(x=WIDTH // 3, y=160)

lab_for_start_of_new_cycle_2 = Label(master=root, text="Введите количество циклов на этот тест:")
lab_for_start_of_new_cycle_2.place(x=WIDTH // 3, y=200)

ent_for_start_of_new_cycle_2 = Entry(master=root, bg="grey")
ent_for_start_of_new_cycle_2.place(x=WIDTH // 3, y=220)

but_for_start_of_new_cycle = Button(master=root, width=13, text="Принять")
but_for_start_of_new_cycle.place(x=WIDTH // 3, y=240)
but_for_start_of_new_cycle.bind('<Button-1>', new_cycle)


# TODO для пущей важности проверить правильность измерения ошибки в мм

# TODO *улучшение вида кода* Сделать в ф-ции stats функции вместо копипаста
# TODO *необязательная фича* сделать файл с настройками
# TODO *необязательная фича* добавить справку с правилами, инструкциями, предостеряжениями
# TODO *после того, как будет сделана вся желаемая статистика* Сделать нормальное ускорение


#                                               ---- START INIT UI ----
#                                               ---- START INIT UI ----
#                                               ---- START INIT UI ----


def translate_from_mm_to_pix(mm, k):
    pix = 100 / mm  # пикселей в 1 мм
    pix *= k  # пикселей в k мм
    return pix


def changes_of_textbox_in_calib(event):
    global SPREAD, VELOCITY_OF_LINE
    if try_to_int(ent_of_window_of_calibration_0):  # for vertical.  Spread
        val = ent_of_window_of_calibration_0.get()
        SPREAD = translate_from_mm_to_pix(int(val), chosen_spread.get())

    if try_to_int(ent_of_window_of_calibration_1):
        val = ent_of_window_of_calibration_1.get()
        VELOCITY_OF_LINE = translate_from_mm_to_pix(int(val) * 60, chosen_vel.get())
        # умножим на 60 т.к. в программе скорость измеряется в пиксель в кадр. Всего ок. 60 кадров в секунду


def changes_of_radiobutton_vel():
    global VELOCITY_OF_LINE
    if try_to_int(ent_of_window_of_calibration_0):
        VELOCITY_OF_LINE = translate_from_mm_to_pix(int(ent_of_window_of_calibration_0.get()) * 60, chosen_vel.get())
        if VELOCITY_OF_LINE < 0:
            pass
            # TODO здесь может быть ваше ускорение


def changes_of_radiobutton_spread():
    global SPREAD
    if try_to_int(ent_of_window_of_calibration_1):
        SPREAD = translate_from_mm_to_pix(int(ent_of_window_of_calibration_1.get()), chosen_spread.get())


def changes_of_vector():
    global CHOSEN_OPTION_OF_LINES, START_POSITION, START_VECTOR
    if chosen_vector.get() == 1:
        CHOSEN_OPTION_OF_LINES = 1

    else:
        CHOSEN_OPTION_OF_LINES = 0
    START_POSITION = [[WIDTH, HEIGHT // 2, WIDTH + LENGTH_OF_LINE, HEIGHT // 2],
                      [0, HEIGHT // 2, -LENGTH_OF_LINE, HEIGHT // 2]]
    START_VECTOR = [1, -1]
    START_POSITION = START_POSITION[CHOSEN_OPTION_OF_LINES]
    START_VECTOR = START_VECTOR[CHOSEN_OPTION_OF_LINES]


# ---- WINDOW OF OPTION ----


# for RadioButtons
chosen_spread = IntVar()
chosen_spread.set(30)

chosen_vel = IntVar()
chosen_vel.set(100)

chosen_vector = IntVar()
chosen_vector.set(0)

Window_of_option = Toplevel(root)
Window_of_option.withdraw()
# With this command when you close the window it actually doesn't close, just roll up
Window_of_option.protocol("WM_DELETE_WINDOW", Window_of_option.withdraw)
Window_of_option.geometry("250x300")

label_height = Label(master=Window_of_option, text="Высота кривой:")
label_height.place(x=0, y=0)

rb_height_1 = Radiobutton(master=Window_of_option, text="30", variable=chosen_spread, value=30,
                          command=changes_of_radiobutton_spread)
rb_height_2 = Radiobutton(master=Window_of_option, text="50", variable=chosen_spread, value=50,
                          command=changes_of_radiobutton_spread)
rb_height_1.place(x=0, y=15)
rb_height_2.place(x=0, y=40)

label_vel = Label(master=Window_of_option, text="Скорость кривой:")
label_vel.place(x=0, y=60)

rb_vel_1 = Radiobutton(master=Window_of_option, text="100 мм/с", variable=chosen_vel, value=100,
                       command=changes_of_radiobutton_vel)
rb_vel_2 = Radiobutton(master=Window_of_option, text="150 мм/с", variable=chosen_vel, value=150,
                       command=changes_of_radiobutton_vel)
rb_vel_3 = Radiobutton(master=Window_of_option, text="с ускорением от 100 мм/с до 150 мм/с", variable=chosen_vel,
                       value=-1, command=changes_of_radiobutton_vel)

rb_vel_1.place(x=0, y=85)
rb_vel_2.place(x=0, y=110)
rb_vel_3.place(x=0, y=135)

label_of_vector = Label(master=Window_of_option, text="Направление движения линий:")
label_of_vector.place(x=0, y=170)

rb_vector_1 = Radiobutton(master=Window_of_option, text="Справа налево <---", variable=chosen_vector, value=0,
                          command=changes_of_vector)
rb_vector_2 = Radiobutton(master=Window_of_option, text="Слева направо --->", variable=chosen_vector, value=1,
                          command=changes_of_vector)

rb_vector_1.place(x=0, y=195)
rb_vector_2.place(x=0, y=220)

# ---- WINDOW OF CALIBRATION ----


Window_of_calibration = Toplevel(root)
Window_of_calibration.withdraw()
Window_of_calibration.protocol("WM_DELETE_WINDOW", Window_of_calibration.withdraw)
Window_of_calibration.geometry("140x190")

canv_calib = Canvas(Window_of_calibration, width=100, height=100, bg="White")
canv_calib.pack()
l = Line(canv_calib, [50, 0, 50, 100])
l2 = Line(canv_calib, [0, 50, 100, 50])

ent_of_window_of_calibration_0 = Entry(Window_of_calibration, width=16, bg="Grey", textvariable=StringVar(value="26"))
ent_of_window_of_calibration_1 = Entry(Window_of_calibration, width=16, bg="Grey", textvariable=StringVar(value="26"))
ent_of_window_of_calibration_0.place(x=0, y=120)
ent_of_window_of_calibration_1.place(x=0, y=150)

but0 = Button(master=Window_of_calibration, width=13, text="Accept changes")
but0.bind('<Button-1>', changes_of_textbox_in_calib)
but0.place(y=170)


def set_ui_in_window_of_option():
    ent_of_window_of_calibration_0.insert(0, str(VELOCITY_OF_LINE))
    ent_of_window_of_calibration_0.place(x=0, y=20)

    label1 = Label(text="WIDTH:")
    label1.place(x=0, y=40)

    # but0 = Button(width=13, text="Accept changes")
    # but0.bind('<Button-1>', changes)
    # but0.place(y=HEIGHT / 2 - 20)


# ---- INIT MENU ----
menubar = Menu(root)
root.config(menu=menubar)
print(root.keys())

fill_of_menu = Menu(menubar)
fill_of_menu.add_command(label="Настройки тестов", command=Window_of_option.deiconify)
fill_of_menu.add_command(label="Калибровка", command=Window_of_calibration.deiconify)
fill_of_menu.add_command(label="Выход", command=exit)

menubar.add_cascade(label="Опции", menu=fill_of_menu)

menubar.add_command(label="Среднее по циклам", command=average_stat_cycles_of_all_subjects)

# --- START OF PROGRAM ---

init_lists()  # Создадим все нужные массивы и храним их в отдельном классе
controller = Controller(canv)
check_directory_for_tests()  # Создадим все нужные дериктории для тестов
root.geometry("800x600")
root.resizable(False, False)  # запрет на изменение окна

root.mainloop()
