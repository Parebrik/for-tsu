#  ver 0.5.1
# TODO Animation of bullet; spawn new small asts, if 2 asts was bump into themselves
import SimpleGUICS2Pygame.simpleguics2pygame as simp
import math
import random
WIDTH = 1000
HEIGHT = 800

ACCEL_SHIP = 0.14
ANGEL_SHIP = 0.0175 * 3
time_of_frame = 0

points = 0


class ImageInformation:
    def __init__(self, path_of_im, obj_size, im_size):
        self.image = simp.load_image(path_of_im)
        print(self.image.get_width())
        self.obj_size = obj_size
        self.im_size = im_size
        self.im_center = [im_size[0] / 2, im_size[1] / 2]

    def get_image(self):
        return self.image

    def get_obs(self):
        return self.obj_size

    def get_ims(self):
        return self.im_size

    def get_imc(self):
        return self.im_center


SHIP_IM = ImageInformation(path_of_im=r"Q:\The_Game_Asteroids\ship3.png", obj_size=[90, 90], im_size=[1287, 850])
ASTEROID_1_IM = ImageInformation(path_of_im=r"Q:\The_Game_Asteroids\Asteroid.png", obj_size=[50, 50], im_size=[512, 512])
Engine_animation = ImageInformation(path_of_im=r"Q:\The_Game_Asteroids\fire_animation.png", obj_size=[50, 50], im_size=[512, 512])
BULLET_IM = ImageInformation(path_of_im=r"Q:\The_Game_Asteroids\bullet.png", obj_size=[20, 10], im_size=[1280, 1280])
BULLET_IM_ANIM = ImageInformation(path_of_im=r"Q:\The_Game_Asteroids\bullet_animation.png", obj_size=[20, 10], im_size=[2048, 1024])
# 512x512 one sprite of fire
# "https://gitlab.com/Parebrik/for-tsu/raw/master/The_Game_Asteroids/Asteroid.png"


class Player:
    def __init__(self):
        self.image = SHIP_IM

        self.lifes_of_ship = 3 ** 100
        self.END_GAME = False

        self.posi = [WIDTH//2, HEIGHT//2]

        self.dif_ang_r = 0
        self.dif_ang_l = 0

        self.angel = math.radians(90)  # In Radians

        self.accel_f = 0
        self.accel_b = 0

        self.vel_2 = [0, 0]
        self.friction = 0.99
        self.direction = [0, 0]

        self.Trust = False

        self.cool_down_of_shoot = 15  # был ли выстрел; сколько времени прошло после этого

    def get_self_lifes_of_ship(self):
        return self.lifes_of_ship

    def get_pos(self):
        return self.posi

    def get_radius(self):
        return int(self.image.get_obs()[0] / 2)

    def animation_of_fire(self, canvas, frame):

        center_fire = [0, 0]
        center_fire[0] = 256 + frame[0] * 512  # центр по х
        center_fire[1] = 256 + frame[1] * 512  # центр по y

        k = SHIP_IM.get_obs()[1] / 2
        k *= 1.2  # чуть-чуть сдвинуть огонь, чтобы его больше было видать
        # отрисовка огня, при движении вперёд
        if self.Trust and self.accel_f > 0:
            pos_fire = [self.posi[0] - (math.cos(self.angel) * k), self.posi[1] + (math.sin(self.angel) * k)]
            canvas.draw_image(Engine_animation.get_image(), center_fire, Engine_animation.get_ims(),
                              pos_fire, Engine_animation.get_obs(), -self.angel)

        # отрисовка огня, при движении назад
        if self.Trust and abs(self.accel_b) > 0:

            kx = 33 * math.sin(self.angel)
            ky = 33 * math.cos(self.angel)
            pos_fire_l = [(self.posi[0] - kx),
                          (self.posi[1] - ky)]

            pos_fire_r = [(self.posi[0] + kx),
                          (self.posi[1] + ky)]

            canvas.draw_image(Engine_animation.get_image(), center_fire, Engine_animation.get_ims(),
                              pos_fire_l, [20, 20], -self.angel - math.pi)

            canvas.draw_image(Engine_animation.get_image(), center_fire, Engine_animation.get_ims(),
                              pos_fire_r, [20, 20], -self.angel - math.pi)

    def draw(self, canvas, frame):
        if not self.END_GAME:
            self.update()
            self.animation_of_fire(canvas, frame)

            canvas.draw_image(self.image.get_image(), self.image.get_imc(), self.image.get_ims(),
                              self.posi, self.image.get_obs(), -self.angel)
        else:
            canvas.draw_text("Игра Окончена", [WIDTH / 2 - 50, HEIGHT / 2], 25, 'Red')
            timer_spawn.stop()

    def update(self):
        self.angel += self.dif_ang_l + self.dif_ang_r

        if self.Trust:
            self.direction = [math.cos(self.angel), -(math.sin(self.angel))]

        self.vel_2[0] += self.accel_b * self.direction[0] + self.accel_f * self.direction[0]
        self.vel_2[1] += self.accel_b * self.direction[1] + self.accel_f * self.direction[1]

        self.vel_2[0] *= self.friction
        self.vel_2[1] *= self.friction

        self.posi[0] += self.vel_2[0]
        self.posi[1] += self.vel_2[1]

        self.teleport()

        self.cool_down_of_shoot += 1

    def key_up(self, key):
        if key == simp.KEY_MAP["w"]:
            self.accel_f = 0
            if not abs(self.accel_b) > 0:
                self.Trust = False

        if key == simp.KEY_MAP["s"]:
            self.accel_b = 0
            if not abs(self.accel_f) > 0:
                self.Trust = False

        if key == simp.KEY_MAP["a"]:
            self.dif_ang_l = 0

        if key == simp.KEY_MAP["d"]:
            self.dif_ang_r = 0

    def key_down(self, key):
        if key == simp.KEY_MAP["w"]:
            self.Trust = True
            self.accel_f = ACCEL_SHIP

        if key == simp.KEY_MAP["s"]:
            self.Trust = True
            self.accel_b = -ACCEL_SHIP + ACCEL_SHIP / 1.5

        if key == simp.KEY_MAP["a"]:
            self.dif_ang_l = ANGEL_SHIP

        if key == simp.KEY_MAP["d"]:
            self.dif_ang_r = -ANGEL_SHIP

        if key == simp.KEY_MAP["f"]:
            if self.cool_down_of_shoot >= 24:
                self.cool_down_of_shoot = 0
                rocket = Rocket(self.vel_2, self.posi, self.angel)
                set_of_rockets.add(rocket)

    def teleport(self):
        if self.posi[0] < 0:
            self.posi[0] = WIDTH - 10
        if self.posi[0] > WIDTH:
            self.posi[0] = 10

        if self.posi[1] < 0:
            self.posi[1] = HEIGHT - 10
        if self.posi[1] > HEIGHT:
            self.posi[1] = 10

    def damage(self):
        self.lifes_of_ship -= 1
        if self.lifes_of_ship <= 0:
            self.END_GAME = True


class Asteroid:
    def __init__(self, posi, ship_posi, angel, image=ASTEROID_1_IM):
        self.image = image
        self.posi = posi  # TODO random Function, which choose the coordinates of spawn, the direction will be ship
        self.vel = 4
        self.angel = angel
        self.ship_posi = ship_posi
        self.direction = [math.cos(angel), -(math.sin(angel))]

    def get_radius(self):
        return int(self.image.get_obs()[0] / 2)

    def get_pos(self):
        return self.posi

    def teleport(self):
        if self.posi[0] < 0:
            self.posi[0] = WIDTH - 10
        if self.posi[0] > WIDTH:
            self.posi[0] = 10

        if self.posi[1] < 0:
            self.posi[1] = HEIGHT - 10
        if self.posi[1] > HEIGHT:
            self.posi[1] = 10

    def update(self, canvas):
        self.posi[0] += self.vel * self.direction[0]
        self.posi[1] += self.vel * self.direction[1]
        self.teleport()
        canvas.draw_image(self.image.get_image(), self.image.get_imc(), self.image.get_ims(), self.posi, self.image.get_obs(), -self.angel)

    def destroy(self, list):
        list.remove(self)


class Rocket:
    def __init__(self, vel, posi, angel):
        self.life_time = 0
        self.angel = angel
        self.image = BULLET_IM
        self.vel = 13
        self.vel += vel[0]
        k = SHIP_IM.get_obs()[1] / 2
        self.posi = [posi[0] + (math.cos(self.angel) * k), posi[1] - (math.sin(self.angel) * k)]

        self.direction = [math.cos(self.angel), -(math.sin(self.angel))]

    def get_radius(self):
        return self.image.get_obs()[0] / 2

    def get_pos(self):
        return self.posi

    def update(self, canvas):
        if self.life_time > 45:
            set_of_rockets.remove(self)
        else:
            self.life_time += 1

        self.posi[0] += self.vel * self.direction[0]
        self.posi[1] += self.vel * self.direction[1]

        self.teleport()
        canvas.draw_image(self.image.get_image(), self.image.get_imc(), self.image.get_ims(), self.posi, self.image.get_obs(), -self.angel)

    def teleport(self):
        if self.posi[0] < 0:
            self.posi[0] = WIDTH - 10
        if self.posi[0] > WIDTH:
            self.posi[0] = 10

        if self.posi[1] < 0:
            self.posi[1] = HEIGHT - 10
        if self.posi[1] > HEIGHT:
            self.posi[1] = 10

    def destroy(self, list):
        list.remove(self)


def spawn_asteroids(posi_s):
    size = ASTEROID_1_IM.get_obs()[0] + 10
    for _ in range(0, 1):
        angel = math.radians(random.randrange(30, 300))
        x = random.randrange(0, 4)

        if x == 0:
            set_of_asteroids.add(Asteroid([WIDTH + size, random.randrange(0, 600)], posi_s, angel=angel))
        if x == 1:
            set_of_asteroids.add(Asteroid([random.randrange(0, 600), -size], posi_s, angel=angel))
        if x == 2:
            set_of_asteroids.add(Asteroid([-size, random.randrange(0, 600)], posi_s, angel=angel))
        if x == 3:
            set_of_asteroids.add(Asteroid([random.randrange(0, 600), HEIGHT + size], posi_s, angel=angel))


def collision(obj1, obj2):  # первый объект- корабль или астероид, второй- астероид или ракета

    def distance(a, b):
        k1 = abs(a[0] - b[0])
        k2 = abs(a[1] - b[1])

        res = (k1 ** 2 + k2 ** 2) ** 0.5
        return res

    r1 = obj1.get_radius()
    r2 = obj2.get_radius()

    pos1 = obj1.get_pos()
    pos2 = obj2.get_pos()

    if distance(pos1, pos2) <= r1 + r2:
        return True


def timer_hand():
    spawn_asteroids(SHIP.get_pos())


def draw(canvas):
    global time_of_frame, points, lifes_of_ship
    frame = [0, 0]
    frame[0] = time_of_frame // 4
    frame[1] = time_of_frame % 4

    copy_of_set_of_rockets = set(set_of_rockets)

    SHIP.draw(canvas, frame)

    for ast in set_of_asteroids:
        ast.update(canvas)

    for rocket in copy_of_set_of_rockets:
        rocket.update(canvas)

    canvas.draw_text(str(points), [WIDTH - WIDTH / 4, 30], 12, 'Red')

    canvas.draw_text("Количество жизней " + str(SHIP.get_self_lifes_of_ship()), [WIDTH / 4, 30], 12, 'Red')

    copy_of_set_of_asteroids = set(set_of_asteroids)
    copy_of_set_of_rockets = set(set_of_rockets)

    for ast in copy_of_set_of_asteroids:
        if collision(SHIP, ast):
            SHIP.damage()
            set_of_asteroids.remove(ast)

        for rocket in copy_of_set_of_rockets:
            if collision(rocket, ast):
                ast.destroy(set_of_asteroids)
                rocket.destroy(set_of_rockets)
                rocket.destroy(copy_of_set_of_rockets)

                points += 1
                break

        for ast_2 in copy_of_set_of_asteroids:
            if ast != ast_2:
                if collision(ast, ast_2):
                    ast_2.destroy(set_of_asteroids)
                    # Delete just one ast, cos in the next iterations another ast will be in variable ast_2
                    break

    time_of_frame += 1
    time_of_frame %= 16


root = simp.create_frame("Asteroids", WIDTH, HEIGHT)
root.set_canvas_background("White")
root.set_draw_handler(draw)

SHIP = Player()

set_of_asteroids = set()
set_of_rockets = set()

spawn_asteroids(SHIP.get_pos())
root.set_keyup_handler(SHIP.key_up)
root.set_keydown_handler(SHIP.key_down)

timer_spawn = simp.Timer(1000, timer_hand)
timer_spawn.start()

root.start()
