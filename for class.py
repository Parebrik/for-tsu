def translate_in_4(n, k):
    d = {"0": 'A', "1": "C", "2": "G", "3": "T"}
    n = int(n)
    k = int(k)
    new = ""
    newnew = ""
    while n > 0:
        ost = n % 4
        new += str(ost)
        n //= 4
    new = new[::-1]
    # new = f"{'0' * (k - len(new))}{new}"
    new = "0" * (k - len(new)) + new
    for let in new:
        newnew += d[let]
        # print(d[let], end="")


def translate_in_10(dna):
    d = {"A": '0', "C": "1", "G": "2", "T": "3"}
    num_in_4 = ""
    for let in dna:
        num_in_4 += d[let]
    res = int(num_in_4, 4)
    return res


class NewDict(dict):
    def up(self, word, inc):
        if word not in self.keys():
            self.update({word: 1})
        else:
            self[word] += inc


def Main_func(genome, k, L, t):
    _list = [0] * 4 ** k
    res = []
    d = NewDict()

    for i in range(0, L - k):
        # ind = translate_in_10(genome[i:i + k])
        # _list[ind] += 1
        # if genome[i:i + k] not in d.keys():
        #     d.update({genome[i:i + k]: 1})
        # else:
        #     d[genome[i:i + k]] += 1
        d.up(genome[i:i + k], 1)
    print(d)
    print("\n")
    for i in range(L - k, len(genome) - k):
        window_gen = genome[i:i + L]
        d.up(window_gen[:k], 1)
        if d[window_gen[:k]] >= t:
            to_res = window_gen[:k]
            res.append(to_res)

        if len(window_gen) >= L:
            d.up(window_gen[-k:], 1)
            if d[window_gen[-k:]] >= t:
                to_res = window_gen[-k:]
                res.append(to_res)

        d.up(genome[i - 1:i + k - 1], -1)
        print(d)
    print(res)


# gen = input()
# inp = list(map(int, input().split()))

gen = "CGGACTCGACAGATGTGAAGAACGACAATGTGAAGACTCGACACGACAGAGTGAAGAGAAGAGGAAACATTGTAA"
inp = [5, 50, 4]

Main_func(gen, inp[0], inp[1], inp[2])
